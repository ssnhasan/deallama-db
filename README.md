In the YAML we are setting the following key values:
      MYSQL_DATABASE: 'deallama_db'
      MYSQL_USER: 'user'
      MYSQL_PASSWORD: 'password'
      MYSQL_ROOT_PASSWORD: 'rootpassword'

Once you start this up using docker-compose up, you should be able to connect to it using something like SQL workbench:
Name: whateverYouWant
Method: Standard (TCP/IP)
Hostname: 127.0.0.1
Port: 3306
Username: user (or root, as above)
Password: password (or rootpassword, as above)

Before using this, please ensure that the user is granted the required permissions (configured in application.properties for now; you must login as root to make this change):
create user 'springuser'@'%' identified by 'p@ssw0rd';
grant all on deallama_db.* to 'springuser'@'%';
